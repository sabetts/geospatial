/**
 * @license
 * Copyright 2018,2019 Shawn Betts
 * SPDX-License-Identifier: MIT
**/

function Config() {
    this.player = {startPosition: {x: 200.0, y: 200.0},
                   startVelocity: {x:0.0, y:0.0},
                   startAngle: 0,
                   turnRate: 3,
                   // acceleration: 0.02,
                   acceleration: 0.04,
                   maxSpeed: 3,
                   collisionRadius: 10,
                   deathTimer: 120,
                   colors: ['#FFFF00', '#00FFFF', '#FF3333', '#FF00FF']
                  };
    this.fortress = { lockTime: 120,
                      smallHex: 40,
                      bigHex: 170,
                      collisionRadius: 18,
                      speed: 0.25,
                      lockSpeed: 5,
                      respawnTime: 30*60};
    this.missile = { collisionRadius: 5,
                     speed: 3,
                     lifespan: 90};
    this.shell = { collisionRadius: 3,
                   speed: 1.5,
                   lifespan: 180};
    this.rewards = { shipDeath: -100,
                     fortressDestroy: 100 };
    this.spheres = { spawnQuantity: 3,
                     radius: 20,
                     acceleration: 0.1,
                     speed: 0.8};
    this.pointConversion = 0.10;
    this.message = { duration: 240 };
    this.mapSize = 100;
    this.mapCellSize = 20;
    this.serverUpdateBufferSize = 60;
    this.maxTicks = 60 * 60 * 3;
    this.network = { stateSyncLength: 60 };

    // this.maxTicks = 60 * 10;
}

Config.prototype = {};

(function(exports) {
    exports.Config = Config;
})(typeof exports === 'undefined' ? this['config']={}:exports);
